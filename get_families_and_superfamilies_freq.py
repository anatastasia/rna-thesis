# cWW-cWW is impossible, wtf
import pandas
from collections import defaultdict


def reverse_bp(bp):
    if len(bp) <= 3:
        return bp

    a = bp[0] + bp[2] + bp[1]
    b = bp[3] + bp[5] + bp[4]
    return b + a


def add_elem(dct, a, aa):
    if a in dct and aa not in dct:
        dct[a] += 1
        return

    if a not in dct and aa in dct:
        dct[aa] += 1
        return

    if a not in dct and aa not in dct:
        dct[a] += 1
        return

    if a in dct and aa in dct:
        dct[a] += 1
        return


d = pandas.read_csv("Triples.csv", sep=";")
d = d.fillna("")

stat1 = defaultdict(int)  # w/three bps
stat2 = defaultdict(int)  # w/o three bps

superfams1 = defaultdict(int)
superfams2 = defaultdict(int)

ln = 0

for ind, row in d.iterrows():
    ln += 1
    bp12 = row["BP12"]
    bp23 = row["BP23"]
    bp31 = row["BP31"]

    if all([bp12, bp23, bp31]) == "":
        continue

    if bp12:
        superfams1[bp12] += 1
    if bp23:
        superfams2[bp31] += 1

    s1 = bp12 + bp23
    ss1 = reverse_bp(s1)
    if len(s1) > 3:
        add_elem(stat1, s1, ss1)

    if bp12 and bp23 and bp31:
        add_elem(stat2, s1, ss1)

for i in stat1.iteritems():
    print i[0], ": ", 100. * i[1] / ln
#
# print "------------------------"
#
# for i in stat2.iteritems():
#     print i[0], ": ", 1. * i[1] / ln


print "Most Frequent Family: "
print [i for i, j in stat1.iteritems() if j == max(stat1.values())], 100. * max(stat1.values()) / ln
# print [i for i, j in stat2.iteritems() if j == max(stat2.values())], 100. * max(stat2.values()) / ln


print "Most Frequent Superfamily: "
print [i for i, j in superfams1.iteritems() if j == max(superfams1.values())], 100. * max(superfams1.values()) / ln
# print [i for i, j in superfams2.iteritems() if j == max(superfams2.values())], 100. * max(superfams2.values()) / ln
