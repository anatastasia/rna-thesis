import pymysql
import csv
connection = pymysql.connect(host='localhost',
                             port=1000,
                             user='root',
                             db='rss',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)

dd = open("/home/anata-m/Thesis/doubledata_test.csv", "w")
d = open("/home/anata-m/Thesis/data.csv", "r")

print len(list(d.readlines()))

try:
    with connection.cursor() as cursor:
        outer_result = csv.reader(d)
        header_labels = next(outer_result)
        header = False
        for row in outer_result:
            curr_row = dict(zip(header_labels, row))
            old_row = dict(zip(header_labels, row))

            sql = "select * " \
                  "from BasePairs " \
                  "where model = %s and nucl1 in (%s, %s, %s) " \
                  "and nucl2 in (%s, %s, %s)"
            cursor.execute(sql, (row[1],
                                 row[2][:6], row[2][7:14], row[2][15:22],
                                 row[2][:6], row[2][7:14], row[2][15:22]))
            result = cursor.fetchall()

            if result:
                w = csv.DictWriter(dd, header_labels + result[0].keys())
                if not header:
                    w.writeheader()
                    header = True

                for i in result:
                    curr_row.update(i)
                    w.writerow(curr_row)
                    curr_row = old_row

            exit()
finally:
    connection.close()