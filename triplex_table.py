import pandas
import pymysql

f = open("/home/anata-m/Thesis/doubledata.csv", "r")
data = pandas.read_csv(f)

lumults = data["lumult"].value_counts()
many_bps = []
for ind, num in enumerate(lumults):
    if num > 2:
        many_bps.append((lumults.index[ind], num))

lumult_model = []
for i, _ in many_bps:
    lumult_model.append((i, data.loc[data["lumult"] == i].model.iloc[0]))
lumult_model_df = pandas.DataFrame(lumult_model, columns=["lumult", "model"])

connection = pymysql.connect(host='localhost',
                             port=1000,
                             user='root',
                             db='rss',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)

result = []
try:
    with connection.cursor() as cursor:
        sql = "select id, file, number " \
                  "from models "
        cursor.execute(sql)
        result = pandas.DataFrame(cursor.fetchall())
finally:
    connection.close()

table = pandas.DataFrame(columns=["file", "id", "number", "nucl1", "nucl2", "nucl3", "bps"])
for lumult, num in many_bps:
    model = lumult_model_df.loc[lumult_model_df["lumult"] == lumult].model.iloc[0]
    row = result.loc[result["id"] == model]

    nucls = data.loc[data["lumult"] == lumult].nucls.iloc[0]
    nucl1 = nucls[:6]
    nucl2 = nucls[7:14]
    nucl3 = nucls[15:22]
    row["nucl1"] = nucl1
    row["nucl2"] = nucl2
    row["nucl3"] = nucl3

    row["bps"] = num

    table = table.append(row)

table.index = range(len(table))
pandas.DataFrame.to_csv(table, "/home/anata-m/Thesis/triplexes.csv")
