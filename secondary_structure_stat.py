import pandas
import csv
from collections import defaultdict

# SS1;SS2;SS3;LCLR12;LCLR23;LCLR31

triples = pandas.read_csv("NR_triples.csv", sep=";")
triples = triples.fillna("")

stat = defaultdict(int)
for _, row in triples.iterrows():
    struct = row["SS1"] + "," + row["SS2"] + "," + row["SS3"] + "," + \
             row["LCLR12"] + "," + row["LCLR23"] + "," + row["LCLR31"]
    stat[struct] += 1

stat = sorted(stat.iteritems(), key=lambda x: x[1])

with open("nr_secondary_structure.csv", "w") as csvfile:
    writer = csv.writer(csvfile)
    for i in stat:
        writer.writerow(i)


