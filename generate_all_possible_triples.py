import pandas
from itertools import permutations

perm_dict = {
    0: ["N1", "BP12", "SS1", "LCLR12"],
    1: ["N2", "BP23", "SS2", "LCLR23"],
    2: ["N3", "BP31", "SS3", "LCLR31"],
}


def generate_rows(sr):
    result = pandas.DataFrame(columns=list(sr.index))
    if sr.BP12 and sr.BP23 and sr.BP31:
        perm = permutations([0, 1, 2])
        for i in perm:
            new_triple = sr.Triple[i[0]] + sr.Triple[i[1]] + sr.Triple[i[2]]
            result = result.append(
                pandas.Series(
                    data=[sr.ID, sr.PDB, sr.Chains, new_triple, sr[perm_dict[i[0]][0]], sr[perm_dict[i[1]][0]],
                          sr[perm_dict[i[2]][0]], sr[perm_dict[i[0]][1]], sr[perm_dict[i[1]][1]],
                          sr[perm_dict[i[2]][1]],
                          sr[perm_dict[i[0]][2]],
                          sr[perm_dict[i[1]][2]],
                          sr[perm_dict[i[2]][2]], sr[perm_dict[i[0]][3]], sr[perm_dict[i[1]][3]],
                          sr[perm_dict[i[2]][3]]], index=list(sr.index)), ignore_index=True)
    else:
        new_triple = sr.Triple[::-1]
        bp12 = sr.BP12[0] + sr.BP12[2] + sr.BP12[1]
        bp23 = sr.BP23[0] + sr.BP23[2] + sr.BP23[1]
        result = result.append(sr)
        result = result.append(pandas.Series(data=[sr.ID, sr.PDB, sr.Chains, new_triple,
                                                   sr.N3, sr.N2, sr.N1,
                                                   bp23, bp12, sr.BP31,
                                                   sr.SS3, sr.SS2, sr.SS1,
                                                   sr.LCLR12, sr.LCLR23, sr.LCLR31],
                                             index=list(sr.index)), ignore_index=True)
    return result.drop_duplicates()


def generate_list(sr):
    result = []
    if sr.BP12 and sr.BP23 and sr.BP31:
        perm = permutations([0, 1, 2])
        for i in perm:
            new_triple = sr.Triple[i[0]] + sr.Triple[i[1]] + sr.Triple[i[2]]
            result.append([sr.ID, sr.PDB, sr.Chains, new_triple, sr[perm_dict[i[0]][0]], sr[perm_dict[i[1]][0]],
                           sr[perm_dict[i[2]][0]], sr[perm_dict[i[0]][1]], sr[perm_dict[i[1]][1]],
                           sr[perm_dict[i[2]][1]],
                           sr[perm_dict[i[0]][2]],
                           sr[perm_dict[i[1]][2]],
                           sr[perm_dict[i[2]][2]], sr[perm_dict[i[0]][3]], sr[perm_dict[i[1]][3]],
                           sr[perm_dict[i[2]][3]]])
    else:
        new_triple = sr.Triple[::-1]
        bp12 = sr.BP12[0] + sr.BP12[2] + sr.BP12[1]
        bp23 = sr.BP23[0] + sr.BP23[2] + sr.BP23[1]
        result.append(sr.tolist())
        result.append([sr.ID, sr.PDB, sr.Chains, new_triple,
                       sr.N3, sr.N2, sr.N1,
                       bp23, bp12, sr.BP31,
                       sr.SS3, sr.SS2, sr.SS1,
                       sr.LCLR12, sr.LCLR23, sr.LCLR31])
    return result


if __name__ == "__main__":
    ds = pandas.read_csv("Triples.csv", sep=";")
    ds.fillna("", inplace=True)
    ds = ds[ds.apply(lambda x: len(str(x.BP12) + str(x.BP23) + str(x.BP31)) > 3, axis=1)]
    ds.insert(0, 'ID', range(1, len(ds) + 1))

    # ext_ds = pandas.DataFrame(columns=list(ds))
    final_res = []
    for row in ds.iterrows():
        final_res += generate_list(row[1])
        # possible_rows = generate_rows(row[1])
        # ext_ds = ext_ds.append(possible_rows)

    ext_ds = pandas.DataFrame(final_res, columns=list(ds))
    ext_ds.reset_index()
    ext_ds.drop_duplicates()
    ext_ds.to_csv("multiplied_NR_triples.csv", sep=";")
