import pandas
from collections import defaultdict

yellow_families = {"cWHtWW", "cWScHW", "cHWtSW", "cHHcSW", "cHHtSW", "cHHcWH", "cHHtWH", "tHHtSW", "tHHcWH", "tHHtWH",
                   "cHStWW", "cHScHW", "cHScWH", "cHScHH", "cHStHH", "tHScHW", "tHStHW", "tHScWH", "tHScHH", "cSWcSW",
                   "cSWtSW", "cSWcHH", "cSWtHH", "cSWcSH", "tSWcHW", "tSWcHH", "tSWtHH", "tSWtSH", "cSHcSW", "cSHtSW",
                   "cSHcSH", "cSHtWS", "tSHtSW", "tSHcSH", "tSHtSH", "tSHcWS", "cSScWH", "cSScHH", "tSScWH", "tSScHH"}

gray_families = {"cWWcWW", "tWWcWW", "tWWtWW", "cWHcHW", "tWHcHW", "tWHtHW", "cWScSW", "tWScSW", "tWStSW", "cHWcWW",
                 "cHWtWW", "cHWcWH",
                 "tHWcWW", "tHWtWW", "tHWcWH", "tHWtWH", "cHHcHW", "cHHtHW", "tHHcHW", "tHHtHW", "cHHcHH", "tHHcHH",
                 "tHHtHH", "cHScSW", "cHStSW",
                 "tHScSW", "tHStSW", "cHScSH", "tHScSH", "tHStSH", "cSWcWW", "cSWtWW", "tSWcWW", "tSWtWW", "cSWcWH",
                 "cSWtWH", "tSWcWH", "tSWtWH",
                 "cSWcWS", "tSWcWS", "tSWtWS", "cSHcHW", "cSHtHW", "tSHcHW", "tSHtHW", "cSHcHH", "cSHtHH", "tSHcHH",
                 "tSHtHH", "cSHcHS",
                 "tSHcHS", "tSHtHS", "cSScSW", "cSStSW", "tSScSW", "tSStSW", "cSScSH", "cSStSH", "tSScSH", "tSStSH",
                 "cSScSS", "tSScSS", "tSStSS"}


def reverse_bp(bp):
    if len(bp) <= 3:
        return bp

    a = bp[0] + bp[2] + bp[1]
    b = bp[3] + bp[5] + bp[4]
    return b + a


d = pandas.read_csv("multiplied_NR_triples.csv", sep=";")
d = d.fillna("")

stat1 = defaultdict(list)
ln = 0

for ind, row in d.iterrows():
    ln += 1
    bp12 = row["BP12"]
    bp23 = row["BP23"]
    bp31 = row["BP31"]

    if all([bp12, bp23, bp31]) == "":
        assert False

    if len(bp12 + bp23 + bp31) == 3:
        assert False

    pair = bp12 + bp23
    rev_pair = reverse_bp(pair)

    if pair in yellow_families:
        stat1[pair].append(row.Triple)
        row_to_write = pandas.DataFrame(row).T
        row_to_write = row_to_write.reset_index(drop=True)
        row_to_write = row_to_write.drop('Unnamed: 0', 1)
        row_to_write.to_csv("yellow_triples.csv", mode="a", sep=";", header=False)

    if pair != rev_pair:
        if rev_pair in yellow_families:
            stat1[rev_pair].append(row.Triple)
            row_to_write = pandas.DataFrame(row).T
            row_to_write = row_to_write.reset_index(drop=True)
            row_to_write = row_to_write.drop('Unnamed: 0', 1)
            row_to_write.to_csv("yellow_triples.csv", mode="a", sep=";", header=False)

print stat1
