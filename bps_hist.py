import pandas
import csv
import matplotlib.pyplot

f = open("doubledata.csv", "r")
d = open("data.csv", "r")
n = open("nucls.csv", "r")
b = open("basepairs.csv", "r")

data = pandas.read_csv(f)
bps = data["lumult"].value_counts()

hst = [(i == 1) for i in list(bps)]
print "Form at least one base pair", 1 - (1.0 * sum(hst) / len(hst))

# matplotlib.pyplot.hist(hst, bins=40)
# matplotlib.pyplot.xlabel("# of base pairs in a triplex")
# matplotlib.pyplot.ylabel("# of triplexes")
# matplotlib.pyplot.show()

triples = pandas.read_csv(d, quoting=csv.QUOTE_NONE)
count = 0
prev = -1
all_dssr = set()

for tpl, bps in triples.iterrows():
    count += len(bps)

print "Are part of at least one base triple", count / 4005887.0

count2 = 0
count3 = 0
nucls = pandas.read_csv(n)
bases = pandas.read_csv(b)
for _, y in nucls.iterrows():
    lumult, model, dssr = y
    if pandas.isnull(lumult):
        continue
    x = bases.loc[bases['model'] == model]
    y = len(x.loc[x["nucl1"] == dssr]) + len(x.loc[x["nucl2"] == dssr])
    count2 += (y > 1)
    count3 += (y == 3)

print "participate in more than one base pair simultaneously", count2 / 4005887.0
print "form three base pairs simultaneously", count3 / 4005887.0
# 4005887 nucls

# Form at least one base pair 0.812116840966
# Are part of at least one base triple 0.139355653317
# participate in more than one base pair simultaneously 0.10824693757
# form three base pairs simultaneously 0.0112456991423
