# -*- coding: utf8 -*-
# cWW-cWW is impossible, wtf
import pandas
import numpy as np
import matplotlib.pyplot as plt
from collections import defaultdict

families = ["cWW", "tWW", "cHW", "tHW", "cSW", "tSW",
            "cWH", "tWH", "cHH", "tHH", "cSH", "tSH",
            "cWS", "tWS", "cHS", "tHS", "cSS", "tSS"]


def reverse_bp(bp):
    if len(bp) <= 3:
        return bp

    a = bp[0] + bp[2] + bp[1]
    b = bp[3] + bp[5] + bp[4]
    return b + a


def add_elem(dct, pair):
    if pair[:3] not in families or pair[3:] not in families:
        raise Exception, "invalid BP: {}".format(pair)

    rev_pair = reverse_bp(pair)
    dct[pair] += 1
    dct[rev_pair] += 1

    if pair == rev_pair:
        dct[pair] -= 1


d = pandas.read_csv("NR_triples.csv", sep=";")
d = d.fillna("")

stat1 = defaultdict(int)
ln = 0

for ind, row in d.iterrows():
    ln += 1
    bp12 = row["BP12"]
    bp23 = row["BP23"]
    bp31 = row["BP31"]

    if all([bp12, bp23, bp31]) == "":
        continue

    if len(bp12 + bp23 + bp31) == 3:
        continue

    s123 = bp12 + bp23
    s231 = bp23 + bp31
    s312 = bp31 + bp12
    pairs = [s123, s231, s312]

    for pair in pairs:
        if len(pair) > 3:
            add_elem(stat1, pair)


for i in stat1.iteritems():
    print i[0], ": ", 100. * i[1] / ln


# print "Most Frequent Family: "
# print [i for i, j in stat1.iteritems() if j == max(stat1.values())], 100. * max(stat1.values()) / ln

# авторы смотрели nr датасет!
# делаем табличку как в статье

table = np.zeros((18, 18))
result_csv = open("nr_families_freq.csv", "w")

for x, i in list(enumerate(families)):
    res_str = ""
    for y, j in list(enumerate(families)):
        key = i + j
        table[x][y] = 100. * stat1[key] / ln

        res_str += str(stat1[key]) + "," + str(table[x][y]) + ";"

    result_csv.write(res_str[:-1] + "\n")

nrows, ncols = 18, 18
hcell, wcell = .6, 2.
hpad, wpad = 0, 0
fig = plt.figure(figsize=(ncols * wcell + wpad, nrows * hcell + hpad))
ax = fig.add_subplot(111)
ax.axis('off')
the_table = ax.table(cellText=table, colLabels=families, loc='center', rowLabels=families)
the_table.set_fontsize(20)
the_table.scale(1., 2.)
plt.savefig("nr_table.png")
