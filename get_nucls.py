# -*-coding: utf-8-*-

import pymysql
import csv
connection = pymysql.connect(host='localhost',
                             port=1000,
                             user='root',
                             db='rss',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)

d = open("/home/anata-m/Thesis/work_data.csv", "r")
wd = open("/home/anata-m/Thesis/dssr_data.csv", "w")

# try:
#     with connection.cursor() as cursor:
#         sql = "select n.lumult, n.model, group_concat(n.dssr separator ',') as nucls " \
#               "from nucls n, LuMultiplets m " \
#               "where n.model = m.model and n.lumult = m.id and m.len = 3 " \
#               "group by n.lumult"
#         cursor.execute(sql)
#         outer_result = cursor.fetchall()
#         w = csv.DictWriter(d, ["lumult", "model", "nucls"])
#         w.writeheader()
#         w.writerows(outer_result)
# finally:
#     connection.close()

count = 0
r = csv.reader(d)
w = csv.DictWriter(wd, ["lumult", "model", "nucls", "pos1", "pos2", "pos3", "BP1", "BP2", "BP3"])
w.writeheader()
header_labels = next(r)

for row in r:
    row_dict = dict(zip(header_labels, row))

    if row_dict["nucls"].count(".") == 9:
        nucls = row_dict["nucls"].split(",")
        for pos, nuc in list(enumerate(nucls)):
            with connection.cursor() as cursor:
                sql = "select wing from nucls where model = %s and dssr = %s"
                cursor.execute(sql, (int(row_dict["model"]), nuc))
                result = cursor.fetchall()
                # assert len(result) < 2

                if result:
                    if result[0]["wing"]:
                        row_dict["pos" + str(pos + 1)] = 'S'
                    else:
                        row_dict["pos" + str(pos + 1)] = 'L'
                else:
                    row_dict["pos" + str(pos + 1)] = None

        possible_pairs = [(nucls[0], nucls[1]), (nucls[0], nucls[2]), (nucls[1], nucls[2])]
        for pos, pair in list(enumerate(possible_pairs)):
            with connection.cursor() as cursor:
                sql = "select class2 " \
                      "from BasePairs " \
                      "where model = %s and ((nucl1 = %s and nucl2 = %s))"
                cursor.execute(sql, (int(row_dict["model"]), pair[0], pair[1]))
                result = cursor.fetchall()
                if result:
                    row_dict["BP" + str(pos + 1)] = result[0]["class2"]
                else:
                    row_dict["BP" + str(pos + 1)] = None

                assert len(result) < 2

        w.writerow(row_dict)
        count += 1

print count
