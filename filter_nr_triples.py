import json
import csv

nr_file = json.load(open("ribosome_classes.json", "r"))
allstruct = [item for sublist in nr_file.values() for item in sublist]

triples_file = open("LRLRLR_triples.csv", "r")
reader = csv.reader(triples_file, delimiter=";")
reader.next()

nr_triples = open("magic_triples.csv", "wb")
writer = csv.writer(nr_triples, delimiter=";")
writer.writerow("PDB;Chains;N1;N2;N3;Class;Repr".split(";"))
for row in reader:
    nr_format = row[2][:4].upper() + "|" + row[2][8:] + "|"
    endings = row[3].split(",")
    possibilities = [nr_format + ending for ending in endings]
    for pos in possibilities:
        for ds in nr_file:
            if pos in nr_file[ds]:
                writer.writerow([row[2], row[3], row[5], row[6], row[7], ds, int(pos == nr_file[ds][0])])
                allstruct.remove(pos)


empty_file = open("not_magic_classes.txt", "w")
empty_file.write(", ".join(allstruct))

