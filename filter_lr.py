import pandas as pd

df = pd.read_csv("GAAtSWtHW_triples.csv", sep=";")
df = df[df.apply(lambda x: x.LCLR12 == "LR" and x.LCLR23 == "LR" and x.LCLR31 == "LR", axis=1)]
df = df.drop('Unnamed: 0', 1)
df = df.reset_index(drop=True)
df.to_csv("LRLRLR_triples.csv", sep=";")
