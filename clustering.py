# -*-coding: utf-8-*-

import pandas
import numpy
import seaborn
import re
import matplotlib.pyplot as plt
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.cluster.hierarchical import AgglomerativeClustering
from sklearn.cluster import KMeans
from sklearn.preprocessing import OneHotEncoder
from sklearn.feature_extraction import DictVectorizer
from sklearn.decomposition import TruncatedSVD
from sklearn.base import TransformerMixin
from sklearn.pipeline import make_pipeline
import matplotlib.cm as cm


class RowIterator(TransformerMixin):
    """ Prepare dataframe for DictVectorizer """
    def fit(self, X, y=None):
        return self

    def transform(self, X):
        return (row for _, row in X.iterrows())


def init_maps():
    bps = ['NaN', 'cWW', 'tWH', 'tSW', 'cWH', 'tSH', 'cSH', 'c.W', 'tHW', 'tW.', 'tWW', 'cHH', 'tHH', 'cH.',
           'c.H', 'tHS', 'cWS', 'cHW', 'tWS', 't.W', 'cW.', '...', 'cSW', 'tSS', 'cHS', 'cSS', 't.S', 'tH.',
           't.H', 'c.S', 'tS.', 'cS.']

    bps_map = {bps[i]: i for i in range(len(bps))}

    wing_map = {"S": 1, "L": 2}
    return bps_map, wing_map


def tokenize(string):
    X = []
    tokens = re.split("([a-z].[a-z].[0-9]*.)", string)
    for t in tokens:
        if len(t) > 5:
            X.append(t)

    return X


f = open("/home/anata-m/Thesis/dssr_data.csv", "r")
data = pandas.read_csv(f)
bps_mapping, wing_mapping = init_maps()

data["BP1"].replace(bps_mapping, inplace=True)
data["BP2"].replace(bps_mapping, inplace=True)
data["BP3"].replace(bps_mapping, inplace=True)

data["pos1"].replace(wing_mapping, inplace=True)
data["pos2"].replace(wing_mapping, inplace=True)
data["pos3"].replace(wing_mapping, inplace=True)

data.fillna(0, inplace=True)

vectorizer = CountVectorizer()
# vectorizer = TfidfVectorizer()
y = vectorizer.fit_transform(data["nucls"])
z = pandas.DataFrame(y.toarray())
data = data.drop("nucls", 1)
data = pandas.concat([data, z], axis=1, join='inner')

vectorizer = OneHotEncoder()
y = vectorizer.fit_transform(data)

# kmeans = AgglomerativeClustering(n_clusters=4)
kmeans = KMeans(n_clusters=4)
X = kmeans.fit(y.toarray())
svd = TruncatedSVD(n_components=4)
res = svd.fit_transform(y)

df = pandas.DataFrame(res, columns=["Comp {}".format(i) for i in range(4)])
df["labels"] = X.labels_

print len(df)

pl = seaborn.pairplot(df, hue="labels", vars=["Comp {}".format(i) for i in range(4)])
# pl.set(ylim=(-500, 600), xlim=(-500, 4000))
pl.set(ylim=(-2, 2), xlim=(-2, 2))

seaborn.plt.show()

# TODO: посмотреть вторую статью, выяснить порядок нуклеотидов в триплете
# TODO: гистограмма по нуклеотидам, стемам/петлям/спариваниям
